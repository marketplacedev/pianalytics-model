package com.piqube.pianalytics.vo;

/**
 * Created by jenefar on 22/12/15.
 */
public enum MessageType {

    JOB,
    SHORTLIST,
    INTERVIEW,
    OFFER,
    MESSAGE
}
