package com.piqube.pianalytics.vo;

import java.util.Date;

public class NotificationVO {

	private EmailNotificationVO email;
	private SMSNotificationVO sms;
	private InAppNotificationVO inApp;

	public EmailNotificationVO getEmail() {
		return email;
	}

	public void setEmail(EmailNotificationVO email) {
		this.email = email;
	}

	public SMSNotificationVO getSms() {
		return sms;
	}

	public void setSms(SMSNotificationVO sms) {
		this.sms = sms;
	}

	public InAppNotificationVO getInApp() {
		return inApp;
	}

	public void setInApp(InAppNotificationVO inApp) {
		this.inApp = inApp;
	}
	private String id;
	private String recepient;
	 private String message;
	 private Date sentAt;
	 private Integer type;
	 private String emailId;
	public String getRecepient() {
		return recepient;
	}
	public void setRecepient(String recepient) {
		this.recepient = recepient;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getSentAt() {
		return sentAt;
	}
	public void setSentAt(Date sentAt) {
		this.sentAt = sentAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	 
	 
}
