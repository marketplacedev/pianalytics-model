package com.piqube.pianalytics.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class NotificationModelVO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String firstName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String lastName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String authority;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String adminFirstName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String adminLastName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String uuid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String phoneNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String companyName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String jobId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String jobDesignation;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String minExperience;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String maxExperience;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String jobLocation;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String jobDescription;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String jobRolesAndResponsibilities;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String subject;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String emailVerificationServer;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String emailVerificationServerPort;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String profileId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String shortlistId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String candidateAcceptance;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String candidateDeny;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String consultantReject;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String consultantAcceptance;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String interviewerFeedback;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String userFeedbackRating;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String userFeedbackText;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String profileName;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String serverUrl;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String time;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String deallocationType;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String noOfPositions;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String allocationId;
  
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int minSalaryBand;
         
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int maxSalaryBand;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String interviewRoundName;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String interviewDateTime;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String interviewRoundType;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String profileFilePath;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String recruiterEmail;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String recruiterPhoneNumber;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String screener1Comment;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String screener2Comment;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String screener3Comment;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String canlendarInviteLink;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String onboardTime;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getAdminFirstName() {
        return adminFirstName;
    }

    public void setAdminFirstName(String adminFirstName) {
        this.adminFirstName = adminFirstName;
    }

    public String getAdminLastName() {
        return adminLastName;
    }

    public void setAdminLastName(String adminLastName) {
        this.adminLastName = adminLastName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getEmailVerificationServer() {
        return emailVerificationServer;
    }

    public void setEmailVerificationServer(String emailVerificationServer) {
        this.emailVerificationServer = emailVerificationServer;
    }

    public String getEmailVerificationServerPort() {
        return emailVerificationServerPort;
    }

    public void setEmailVerificationServerPort(String emailVerificationServerPort) {
        this.emailVerificationServerPort = emailVerificationServerPort;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getShortlistId() {
		return shortlistId;
	}

	public void setShortlistId(String shortlistId) {
		this.shortlistId = shortlistId;
	}

	public String getCandidateAcceptance() {
		return candidateAcceptance;
	}

	public void setCandidateAcceptance(String candidateAcceptance) {
		this.candidateAcceptance = candidateAcceptance;
	}

	public String getConsultantReject() {
		return consultantReject;
	}

	public void setConsultantReject(String consultantReject) {
		this.consultantReject = consultantReject;
	}

	public String getInterviewerFeedback() {
		return interviewerFeedback;
	}

	public void setInterviewerFeedback(String interviewerFeedback) {
		this.interviewerFeedback = interviewerFeedback;
	}

	public String getCandidateDeny() {
		return candidateDeny;
	}

	public void setCandidateDeny(String candidateDeny) {
		this.candidateDeny = candidateDeny;
	}

	public String getConsultantAcceptance() {
		return consultantAcceptance;
	}

	public void setConsultantAcceptance(String consultantAcceptance) {
		this.consultantAcceptance = consultantAcceptance;
	}

	public String getJobDesignation() {
		return jobDesignation;
	}

	public void setJobDesignation(String jobDesignation) {
		this.jobDesignation = jobDesignation;
	}

	public String getMinExperience() {
		return minExperience;
	}

	public void setMinExperience(String minExperience) {
		this.minExperience = minExperience;
	}

	public String getMaxExperience() {
		return maxExperience;
	}

	public void setMaxExperience(String maxExperience) {
		this.maxExperience = maxExperience;
	}

	public String getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getJobRolesAndResponsibilities() {
		return jobRolesAndResponsibilities;
	}

	public void setJobRolesAndResponsibilities(String jobRolesAndResponsibilities) {
		this.jobRolesAndResponsibilities = jobRolesAndResponsibilities;
	}

    public String getUserFeedbackRating() {
        return userFeedbackRating;
    }

    public void setUserFeedbackRating(String userFeedbackRating) {
        this.userFeedbackRating = userFeedbackRating;
    }

    public String getUserFeedbackText() {
        return userFeedbackText;
    }

    public void setUserFeedbackText(String userFeedbackText) {
        this.userFeedbackText = userFeedbackText;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeallocationType() {
        return deallocationType;
    }

    public void setDeallocationType(String deallocationType) {
        this.deallocationType = deallocationType;
    }

	public String getNoOfPositions() {
		return noOfPositions;
	}

	public void setNoOfPositions(String noOfPositions) {
		this.noOfPositions = noOfPositions;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public int getMinSalaryBand() {
		return minSalaryBand;
	}

	public void setMinSalaryBand(int minSalaryBand) {
		this.minSalaryBand = minSalaryBand;
	}

	public int getMaxSalaryBand() {
		return maxSalaryBand;
	}

	public void setMaxSalaryBand(int maxSalaryBand) {
		this.maxSalaryBand = maxSalaryBand;
	}

	public String getInterviewRoundName() {
		return interviewRoundName;
	}

	public void setInterviewRoundName(String interviewRoundName) {
		this.interviewRoundName = interviewRoundName;
	}

	public String getInterviewDateTime() {
		return interviewDateTime;
	}

	public void setInterviewDateTime(String interviewDateTime) {
		this.interviewDateTime = interviewDateTime;
	}

	public String getInterviewRoundType() {
		return interviewRoundType;
	}

	public void setInterviewRoundType(String interviewRoundType) {
		this.interviewRoundType = interviewRoundType;
	}

	public String getProfileFilePath() {
		return profileFilePath;
	}

	public void setProfileFilePath(String profileFilePath) {
		this.profileFilePath = profileFilePath;
	}

	public String getRecruiterEmail() {
		return recruiterEmail;
	}

	public void setRecruiterEmail(String recruiterEmail) {
		this.recruiterEmail = recruiterEmail;
	}

	public String getRecruiterPhoneNumber() {
		return recruiterPhoneNumber;
	}

	public void setRecruiterPhoneNumber(String recruiterPhoneNumber) {
		this.recruiterPhoneNumber = recruiterPhoneNumber;
	}

	public String getScreener1Comment() {
		return screener1Comment;
	}

	public void setScreener1Comment(String screener1Comment) {
		this.screener1Comment = screener1Comment;
	}

	public String getScreener2Comment() {
		return screener2Comment;
	}

	public void setScreener2Comment(String screener2Comment) {
		this.screener2Comment = screener2Comment;
	}

	public String getScreener3Comment() {
		return screener3Comment;
	}

	public void setScreener3Comment(String screener3Comment) {
		this.screener3Comment = screener3Comment;
	}

	public String getCanlendarInviteLink() {
		return canlendarInviteLink;
	}

	public void setCanlendarInviteLink(String canlendarInviteLink) {
		this.canlendarInviteLink = canlendarInviteLink;
	}

	public String getOnboardTime() {
		return onboardTime;
	}

	public void setOnboardTime(String onboardTime) {
		this.onboardTime = onboardTime;
	}

	
	
}
