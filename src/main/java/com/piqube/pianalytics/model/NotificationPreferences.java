package com.piqube.pianalytics.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import org.springframework.boot.orm.jpa.EntityScan;

import javax.persistence.*;
import java.util.Date;

/**
 * @author jenefar
 */
@Entity
@EntityScan
@SoftDelete
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "notification_preferences")
public class NotificationPreferences {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    @Column(name = "id")
    private Long id;

    @Column(name="username")
    private String username;

    @Column(name="email")
    private boolean email=true;

    @Column(name="sms")
    private boolean sms=true;

    @Column(name="mobile_app")
    private boolean mobileApp=true;

    @Column(name="web_app")
    private boolean webApp=true;

    @Expose
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isSms() {
        return sms;
    }

    public void setSms(boolean sms) {
        this.sms = sms;
    }

    public boolean isMobileApp() {
        return mobileApp;
    }

    public void setMobileApp(boolean mobileApp) {
        this.mobileApp = mobileApp;
    }

    public boolean isWebApp() {
        return webApp;
    }

    public void setWebApp(boolean webApp) {
        this.webApp = webApp;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
