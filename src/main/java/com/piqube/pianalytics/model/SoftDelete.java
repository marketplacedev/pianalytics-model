/**
 * Filename : SoftDelete.java
 * Description : 
 * Date : 02-Jan-2014
 * Owner : Arul
 * Project : piqube-site-core
 * Contact : me@arulraj.net
 * History : 
 */
package com.piqube.pianalytics.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SoftDelete {

  String ColumnName() default "removedAt";

}
