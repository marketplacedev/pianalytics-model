package com.piqube.pianalytics.model;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.boot.orm.jpa.EntityScan;

import javax.persistence.*;
import java.util.Date;

/**
 * @author jenefar
 */
@Entity
@EntityScan
@SoftDelete
@Table(name = "user_subscription")
public class UserSubscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = true, updatable = true)
    @NotFound(action= NotFoundAction.IGNORE)
    private Users user;

    @Expose
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "access_key_id", nullable = true, updatable = true)
    @NotFound(action= NotFoundAction.IGNORE)
    private AccessKeys accessKey;

    
    @Column(name="rate_limit")
    private Integer rateLimit;

    
    @Column(name="calls_per_day")
    private Integer callsPerDay;

    
    @Column(name="calls_per_month")
    private Integer callsPerMonth;

    @Column(name="score_breakup_availability")
    private Integer scoreBreakupAvailability;

    @Column(name="social_profile_availability")
    private Integer socialProfileAvailability;

    @Column(name="remaining_calls_per_day")
    private Integer remainingCallsPerDay;

    @Column(name="remaining_calls_per_month")
    private Integer remainingCallsPerMonth;

    @Column(name="is_current")
    private Integer isCurrent;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "package_id", nullable = true, updatable = true)
    @NotFound(action= NotFoundAction.IGNORE)
    private SubscriptionPackages subscriptionPackages;

    @Column(name="start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name="end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Expose
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();


    @JoinColumn(name = "removed_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Users removedBy;

    @JoinColumn(name = "updated_by", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedBy;

    @Expose
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @NotFound(action= NotFoundAction.IGNORE)
    private Users createdBy;

    @Column(name = "removed_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date removedAt;
    
    @Expose
    @Column(name = "is_locked")
    private boolean locked;
    
    @Column(name = "is_expired")
    private boolean expired;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public AccessKeys getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(AccessKeys accessKey) {
        this.accessKey = accessKey;
    }

    public Integer getRateLimit() {
        return rateLimit;
    }

    public void setRateLimit(Integer rateLimit) {
        this.rateLimit = rateLimit;
    }

    public Integer getCallsPerDay() {
        return callsPerDay;
    }

    public void setCallsPerDay(Integer callsPerDay) {
        this.callsPerDay = callsPerDay;
    }

    public Integer getCallsPerMonth() {
        return callsPerMonth;
    }

    public void setCallsPerMonth(Integer callsPerMonth) {
        this.callsPerMonth = callsPerMonth;
    }

    public Integer getScoreBreakupAvailability() {
        return scoreBreakupAvailability;
    }

    public void setScoreBreakupAvailability(Integer scoreBreakupAvailability) {
        this.scoreBreakupAvailability = scoreBreakupAvailability;
    }

    public Integer getSocialProfileAvailability() {
        return socialProfileAvailability;
    }

    public void setSocialProfileAvailability(Integer socialProfileAvailability) {
        this.socialProfileAvailability = socialProfileAvailability;
    }

    public Integer getRemainingCallsPerDay() {
        return remainingCallsPerDay;
    }

    public void setRemainingCallsPerDay(Integer remainingCallsPerDay) {
        this.remainingCallsPerDay = remainingCallsPerDay;
    }

    public Integer getRemainingCallsPerMonth() {
        return remainingCallsPerMonth;
    }

    public void setRemainingCallsPerMonth(Integer remainingCallsPerMonth) {
        this.remainingCallsPerMonth = remainingCallsPerMonth;
    }

    public Integer getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(Integer isCurrent) {
        this.isCurrent = isCurrent;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Users getRemovedBy() {
        return removedBy;
    }

    public void setRemovedBy(Users removedBy) {
        this.removedBy = removedBy;
    }

    public Users getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Users updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Users getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Users createdBy) {
        this.createdBy = createdBy;
    }

    public Date getRemovedAt() {
        return removedAt;
    }

    public void setRemovedAt(Date removedAt) {
        this.removedAt = removedAt;
    }

	public SubscriptionPackages getSubscriptionPackages() {
		return subscriptionPackages;
	}

	public void setSubscriptionPackages(SubscriptionPackages subscriptionPackages) {
		this.subscriptionPackages = subscriptionPackages;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	
}
