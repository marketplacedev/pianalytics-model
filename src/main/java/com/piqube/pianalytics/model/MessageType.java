package com.piqube.pianalytics.model;

/**
 * Created by jenefar on 22/12/15.
 */
public enum MessageType {

    JOB,
    SHORTLIST,
    INTERVIEW,
    OFFER,
    MESSAGE
}
