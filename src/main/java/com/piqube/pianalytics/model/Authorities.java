package com.piqube.pianalytics.model;

import org.springframework.boot.orm.jpa.EntityScan;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author jenefar
 */
@Entity
@EntityScan
@Table(name = "authorities")
public class Authorities implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "username", referencedColumnName = "username", nullable = false)
    Users user;
    
    @Column(name="authority")
    private String authority;


    @Column(name = "removed_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date removedAt;

    @JoinColumn(name = "removed_by", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Users removedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Date getRemovedAt() {
        return removedAt;
    }

    public void setRemovedAt(Date removedAt) {
        this.removedAt = removedAt;
    }

    public Users getRemovedBy() {
        return removedBy;
    }

    public void setRemovedBy(Users removedBy) {
        this.removedBy = removedBy;
    }
}
