package com.piqube.pianalytics.model;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author jenefar
 */
@Entity
@Table(name = "user_company")
public class UserCompany implements Serializable {

	private static final long serialVersionUID = -5363915486044406392L;

    @Expose
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Expose
    @Column(name = "name")
    private String companyName;

    @Column(name = "is_freetext")
    private boolean isFreeText;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", nullable = true)
    @NotFound(action= NotFoundAction.IGNORE)
    private Company masterCompany;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

	@Override
    public String toString() {
        return "Company{" +
                ", companyname='" + companyName +
                '}';
    }

	 public boolean isFreeText() {
	        return isFreeText;
	    }

	 public void setIsFreeText(boolean isFreeText) {
	        this.isFreeText = isFreeText;
	    }

		public void setFreeText(boolean isFreeText) {
			this.isFreeText = isFreeText;
		}
		public Company getMasterCompany() {
			return masterCompany;
		}

		public void setMasterCompany(Company masterCompany) {
			this.masterCompany = masterCompany;
		}
	
}
