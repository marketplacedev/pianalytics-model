package com.piqube.pianalytics.model;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.google.gson.annotations.Expose;

import java.awt.TrayIcon.MessageType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Entity
public class Notification{
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Expose
	String id;

	@Expose
	Long user_id;

	@Expose
	String recepient;
	@Expose
	String subject;
	@Expose
	String message;
	String email;
	String phone;
	Integer type;
	Integer status;
	Integer template;
	@Expose
	Date sent_at;
	boolean is_read;
	String model;
	@Transient
	String templateType;
	@Transient
	List<String> recipients=new ArrayList();

	@Expose
	@Transient
	MessageType messageType;

	@Expose
	@Transient
	String jobId;

	@Expose
	@Transient
	String profileId;

	@Expose
	@Transient
	String shortlistId;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}


	
	public enum Type{
		EMAIL(1), SMS(2), MOBILE_APP(3),WEB_APP(4);
		Integer type;

		Type(Integer type) {
			this.type = type;
		}

		public Integer getNotificationType() {
			return type;
		}
	};
	
	public enum Status{
		SUCCESS(1), FAILED(2), PENDING(3);

		Integer statusCode;

		Status(Integer statusCode) {
			this.statusCode = statusCode;
		}

		public Integer getEmailStatus() {
			return statusCode;
		}
	};
	
	public enum Template{
		ONBOARD_VERIFICATION(1),ADMIN_REGISTERED(2),FORGOT_PASSWORD(3);

		Integer templateCode;
		Template(Integer templateCode) {
			this.templateCode = templateCode;
		}
		public Integer getTemplateCode() {
			return templateCode;
		}

	}

	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Notification(){
		
	}

	public String getRecepient() {
		return recepient;
	}

	public void setRecepient(String recepient) {
		this.recepient = recepient;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}


	public Integer getTemplate() {
		return template;
	}

	public void setTemplate(Integer template) {
		this.template = template;
	}

	public boolean is_read() {
		return is_read;
	}

	public void setIs_read(boolean is_read) {
		this.is_read = is_read;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Notification(String recepient, String message, String email,
			String phone, Integer type) {
		super();
		this.recepient = recepient;
		this.message = message;
		this.email = email;
		this.phone = phone;
		this.type = type;
	}

	public Date getSent_at() {
		return sent_at;
	}

	public void setSent_at(Date sent_at) {
		this.sent_at = sent_at;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public boolean isIs_read() {
		return is_read;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getShortlistId() {
		return shortlistId;
	}

	public void setShortlistId(String shortlistId) {
		this.shortlistId = shortlistId;
	}
}