package com.piqube.pianalytics.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.piqube.pianalytics.model.UserCompany;


/**
 * @author jenefar
 */
public interface UserCompanyRepository extends JpaRepository<UserCompany, Long> {

	 

	    UserCompany findByCompanyName(String companyName);

}
