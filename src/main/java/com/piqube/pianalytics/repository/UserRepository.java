package com.piqube.pianalytics.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.piqube.pianalytics.model.Users;

/**
 * Created by jenefar on 28/09/16.
 */
public interface UserRepository extends JpaRepository<Users, Long> {
	
	Users findOneByUsername(String userName);
	
    @Query("select username from Users")
    List<String> findAllEmails();
    
    Users findOneByuUID(String uuid);
}
