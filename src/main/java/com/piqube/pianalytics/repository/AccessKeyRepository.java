package com.piqube.pianalytics.repository;

import com.piqube.pianalytics.model.AccessKeys;
import com.piqube.pianalytics.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author jenefar
 */
public interface AccessKeyRepository extends JpaRepository<AccessKeys, Long> {

    AccessKeys save(AccessKeys accessKeys);
    @Query("select accessKeys from AccessKeys accessKeys where accessKeys.user=?1")
    AccessKeys findByUserId(Users user);
   
	
}

