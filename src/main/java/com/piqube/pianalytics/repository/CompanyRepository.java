package com.piqube.pianalytics.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.piqube.pianalytics.model.Company;

import java.util.List;

/**
 * @author jenefar 
 */
public interface CompanyRepository extends JpaRepository<Company, Long> {

    Company findOneByName(String name);

    @Query("select c from Company c where lower(c.name) like ?1")
    List<Company> findByCompanyNameStartingWith(String query);
}
