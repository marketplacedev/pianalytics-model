package com.piqube.pianalytics.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.piqube.pianalytics.model.Notification;


@Repository
public interface NotificationRepository extends CrudRepository<Notification, String> {
	
	Page<Notification> findAll(Pageable pageable);
	@Query("select s from Notification s where s.recepient =?1  AND s.template IN ?2")
	List<Notification> findByEmailTemplate(String recepient ,List<Integer> emailTemplate);

	Notification findById(String id);
	@Query("select s from Notification s where s.status =?1")
	List<Notification> findByStatus(Integer status);
	@Query("select s from Notification s where s.recepient =?1 AND s.type=?2")
	List<Notification> getNotificationsByType(String emailId, Integer type);

	@Query("select s from Notification s where s.user_id =?1 AND s.type=?2 AND s.sent_at>?3 AND is_read=0")
	List<Notification> findUnreadNotificationsByType(Long userId, Integer type,Date fromDate);

	@Query("select s from Notification s where s.user_id =?1 AND s.type=?2 AND s.sent_at>?3")
	List<Notification> findbyUserIdAndType(Long id, Integer type,Date fromDate);

	@Query("select s from Notification s where s.user_id =?1 AND s.type=?2 AND is_read=0")
	List<Notification> findUnreadbyUserIdAndType(Long userId, Integer type);
}