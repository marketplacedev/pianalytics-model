package com.piqube.pianalytics.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.piqube.pianalytics.model.NotificationPreferences;


public interface NotificationPreferencesRepository extends JpaRepository<NotificationPreferences, Long> {

    NotificationPreferences findOneByUsername(String username);

    NotificationPreferences findByUsername(String email);
}
