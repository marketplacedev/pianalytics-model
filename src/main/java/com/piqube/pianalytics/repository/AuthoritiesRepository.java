package com.piqube.pianalytics.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.piqube.pianalytics.model.Authorities;

/**
 * @author jenefar
 */
public interface AuthoritiesRepository extends JpaRepository<Authorities, Long> {


}
