package com.piqube.pianalytics.repository;

import com.piqube.pianalytics.model.AccessKeys;
import com.piqube.pianalytics.model.UserSubscription;
import com.piqube.pianalytics.model.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author jenefar
 */
public interface UserSubscriptionRepository extends JpaRepository<UserSubscription, Long> {
	
	@Query("select u from UserSubscription u where u.accessKey = ?1")
	UserSubscription findByAccesskey(AccessKeys accessKeys);
	@Query("select u from UserSubscription u where u.user = ?1")
	UserSubscription findByUserId(Users user);
	
}
