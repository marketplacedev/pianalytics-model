package com.piqube.pianalytics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.piqube.pianalytics.model.UserCompany;
import com.piqube.pianalytics.repository.UserCompanyRepository;



/**
 * @author jenefar
 */
@Service
@Transactional
public class UserCompanyServiceImpl implements UserCompanyService {

	 @Autowired
	 UserCompanyRepository userCompanyRepository;
	 
	@Override
    public UserCompany findByCompanyName(String companyName) {
        return userCompanyRepository.findByCompanyName(companyName);
    }

	@Override
	public UserCompany createCompany(UserCompany company) {
		// TODO Auto-generated method stub
		return userCompanyRepository.save(company);
	}


   
}
