package com.piqube.pianalytics.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author girija
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.piqube.pianalytics.model.Authorities;
import com.piqube.pianalytics.model.NotificationPreferences;
import com.piqube.pianalytics.model.Users;
import com.piqube.pianalytics.repository.NotificationPreferencesRepository;
import com.piqube.pianalytics.repository.UserRepository;


@Service
@Transactional
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AuthoritiesService authoritiesService;
	
	@Autowired
	private NotificationPreferencesRepository notificationPreferencesRepository;

	@Override
	public List<String> findAllEmails() {
		return userRepository.findAllEmails();
	}

	public Users getUserByEmail(String email) {
		Users user = userRepository.findOneByUsername(email);
		return user;
	}

	@Override
	public Users createUser(Users user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}
	@Override
	public Users findUserByUUID(String uuid) {
		return userRepository.findOneByuUID(uuid);
	}
	
	@Override
	public Users updateUser(Users user) {
		return userRepository.save(user);
	}

	@Override
	public String removeUser(Users user, Users adminUsername) {

    	String email = user.getUsername();
    	 String tempUserName = user.getUsername() + "+" + "deleted";
    	Set<Authorities> authorities = authoritiesService.findByUsername(email);
       
        user.setUsername(tempUserName);
        user.setAccountEnabled(false);
        user.setRemovedAt(new Date());
        user.setRemovedBy(adminUsername);
        user.setUpdatedBy(user);
        user.setUpdatedBy(adminUsername);
        user.setAccountLocked(true);
        
        
        for (Authorities authority : authorities) {
			authority.setUser(user);
			authority.setRemovedAt(new Date());
			authority.setRemovedBy(adminUsername);
			authoritiesService.saveAuthority(authority);
		}
        userRepository.save(user);
        NotificationPreferences notificationPreferences=notificationPreferencesRepository.findByUsername(email);
        notificationPreferences.setUsername(tempUserName);
        notificationPreferences.setEmail(false);
        notificationPreferences.setUpdatedAt(new Date());
        notificationPreferences.setMobileApp(false);
        notificationPreferences.setSms(false);
        notificationPreferences.setWebApp(false);
        notificationPreferencesRepository.save(notificationPreferences);
        
        return "SUCCESS";
    }
	
}
