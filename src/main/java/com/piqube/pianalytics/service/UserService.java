package com.piqube.pianalytics.service;

import java.util.List;

import com.piqube.pianalytics.model.Users;

public interface UserService {

	  List<String> findAllEmails();
	  Users getUserByEmail(String email);
      Users createUser(Users user);
      Users findUserByUUID(String uuid);
      Users updateUser(Users user);
      String removeUser(Users user,Users adminUsername);

  
}

