package com.piqube.pianalytics.service;

import com.piqube.pianalytics.model.Company;

/**
 * @author jenefar
 */
public interface CompanyService {
    Company findByCompanyName(String name);

}
