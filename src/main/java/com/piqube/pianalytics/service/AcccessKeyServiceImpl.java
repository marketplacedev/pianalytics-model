package com.piqube.pianalytics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.piqube.pianalytics.model.AccessKeys;
import com.piqube.pianalytics.model.Users;
import com.piqube.pianalytics.repository.AccessKeyRepository;


@Service
@Transactional
public class AcccessKeyServiceImpl implements AccessKeyService{

	
	@Autowired
	private AccessKeyRepository accessKeyRepository;
	
	@Override
	public AccessKeys getUserByUserId(Users user) {
		// TODO Auto-generated method stub
		return accessKeyRepository.findByUserId(user);
	}

	@Override
	public AccessKeys updateAccessKey(AccessKeys accessKeys) {
		// TODO Auto-generated method stub
		return accessKeyRepository.save(accessKeys);
	}
}
