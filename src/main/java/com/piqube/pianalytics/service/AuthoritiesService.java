package com.piqube.pianalytics.service;

import java.util.Set;

import com.piqube.pianalytics.model.Authorities;



/**
 * @author jenefar
 */
public interface AuthoritiesService {
    void saveAuthority(Authorities authorities);
    Set<Authorities> findByUsername(String username);
}
