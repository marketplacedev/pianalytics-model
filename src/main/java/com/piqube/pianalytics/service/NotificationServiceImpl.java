package com.piqube.pianalytics.service;


import com.google.gson.Gson;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.piqube.marketplace.util.Helper;
import com.piqube.pianalytics.model.Notification;
import com.piqube.pianalytics.model.NotificationPreferences;
import com.piqube.pianalytics.model.NotificationType;
import com.piqube.pianalytics.model.Users;
import com.piqube.pianalytics.repository.NotificationPreferencesRepository;
import com.piqube.pianalytics.repository.NotificationRepository;
import com.piqube.pianalytics.vo.NotificationMessageVO;
import com.piqube.pianalytics.vo.NotificationVO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.ListUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.velocity.app.VelocityEngine;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
@EnableAsync
public class NotificationServiceImpl implements NotificationService{

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);

    @Autowired
    private NotificationRepository notificationRepository;
    
    @Autowired
    private NotificationPreferencesRepository notificationPreferencesRepository;
    
    @Value("${notification.json}")
    String notificationJsonProperties;
    
    @Autowired
    private VelocityEngine engine;
    
    @Autowired
    private UserService userService;
    

    
    @Override
    @Async
    public String sendNotification(Notification notification) {

        Helper helper=new Helper();
        String notificationJson = helper.readNotificationProperites(notificationJsonProperties);
        JSONObject object = (JSONObject) JSONValue.parse(notificationJson);
        Object template = object.get(notification.getTemplateType());
        System.out.println("in the notificationimpl----"+template.toString());
        NotificationVO notificationProperties = (NotificationVO) Helper.convertJsonToJava(template.toString(), NotificationVO.class);
        LOGGER.info("====Going to call notification preferences====="+notificationProperties.getEmail().getRecipients());

        NotificationPreferences notificationPreferences=notificationPreferencesRepository.findByUsername(notification.getEmail());

        if(null== notificationPreferences) {
            LOGGER.info("====Notification preferences not found=====");
            NotificationPreferences preferences = new NotificationPreferences();
            preferences.setUsername(notification.getEmail());
            preferences.setCreatedAt(new Date());
            notificationPreferences=notificationPreferencesRepository.saveAndFlush(preferences);

        }
        if(null!=notificationPreferences) {
            if (notificationProperties.getEmail().isAvailable() && notificationPreferences.isEmail()) {
            	LOGGER.info("===(email)===notification.getRecipients()>>>"+notification.getRecipients());
            	LOGGER.info("===(email)===notificationProperties.getInApp().getRecipients()>>>"+notificationProperties.getInApp().getRecipients());
                List recipientsList = ListUtils.union(notification.getRecipients(), notificationProperties.getEmail().getRecipients());
                LOGGER.info("===(email)===Recipients list:{}",recipientsList);
                for (Object recipient : recipientsList) {
                    notification.setRecepient(recipient.toString());
                    sendEmailNotification(notification, notificationProperties);
                }
            }
//            if (notificationProperties.getSms().isAvailable() && notificationPreferences.isSms()) {
//                List recipientsList = ListUtils.union(notification.getRecipients(), notificationProperties.getSms().getRecipients());
//
//                for (Object recipient : recipientsList) {
//                    notification.setRecepient(recipient.toString());
//                    sendSMSNotification(notification, notificationProperties);
//                }
//            }

            LOGGER.info("notificationProperties.getInApp().isAvailable:::: {}", notificationProperties.getInApp().isAvailable());
            if (notificationProperties.getInApp().isAvailable()) {
            	LOGGER.info("===(inapp)===notification.getRecipients()>>>"+notification.getRecipients());
            	LOGGER.info("===(inapp)===notificationProperties.getInApp().getRecipients()>>>"+notificationProperties.getInApp().getRecipients());
                LOGGER.info("===(inapp)===going to send notification======");
                List recipientsList = ListUtils.union(notification.getRecipients(), notificationProperties.getInApp().getRecipients());
                LOGGER.info("==(inapp)==recipients list:{}",recipientsList.toString());
                for (Object recipient : recipientsList) {
                    notification.setRecepient(recipient.toString());
//                    sendInAppNotification(notification, notificationProperties, notificationPreferences.isMobileApp());
                }
            }
        }
        else{
            LOGGER.error("No notification preferences found to send notifications!");
        }
        return notificationJson;

    }

   
    public void sendEmailNotification(Notification notification, NotificationVO notificationProperties) {

        LOGGER.info("Sending email to{}",notification.getRecepient());
        notification.setId("");
        notification.setSent_at(new Date());
        notification.setStatus(Notification.Status.PENDING.getEmailStatus());
        notification.setSubject(notificationProperties.getEmail().getSubject());
        notification.setType(Notification.Type.EMAIL.getNotificationType());
        Notification createdNotification = null;
        if(!notification.getRecepient().contains("qube@piqube.com")) {
            //createdNotification = notificationRepository.save(notification);
            createdNotification =saveNotification(notification);
        }
        String messageFromTemplate = VelocityEngineUtils.mergeTemplateIntoString(this.engine, notificationProperties.getEmail().getTemplate(), "UTF-8", Helper.convertJsonStringTOMap(notification.getModel()));
        notification.setMessage(messageFromTemplate);

        sendMail(notification, notificationProperties.getEmail().isOfficial());
        notification.setMessage(null);
        if(null!=createdNotification) {
            Notification updatedNotification = notificationRepository.findById(createdNotification.getId());
            updatedNotification.setStatus(Notification.Status.SUCCESS.getEmailStatus());
            Users usr=userService.getUserByEmail(notification.getRecepient());
            if(null!=usr)
                updatedNotification.setUser_id(usr.getId());

            //notificationRepository.save(updatedNotification);
            saveNotification(updatedNotification);
        }

        LOGGER.info("Email successfully sent to{}",notification.getRecepient());

    }

    public void sendMail(Notification notification,boolean isOfficial){

        MandrillMessage message = new MandrillMessage();
        message.setAutoText(true);
        MandrillApi mandrillApi;

        if(isOfficial) {
            mandrillApi = new MandrillApi("459YYFiKRBwaSS3F9b3zOw");
           // mandrillApi = new MandrillApi("VbCS0PHGxEtLZV9wboKWaQ");
            message.setFromEmail("jay@piqube.com");
            message.setFromName("Jayadev");
        }
        else{
            System.out.println("*****mail is not official");
            mandrillApi = new MandrillApi("459YYFiKRBwaSS3F9b3zOw");
            message.setFromEmail("qube@piqube.com");
            message.setFromName("PiQube");

        }
        ArrayList<MandrillMessage.Recipient> recipients = new ArrayList<MandrillMessage.Recipient>();
        MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
        recipient.setEmail(notification.getRecepient());
        recipients.add(recipient);
       // recipient = new MandrillMessage.Recipient();
       // recipients.add(recipient);
        message.setTo(recipients);
        message.setPreserveRecipients(true);
        message.setHtml(notification.getMessage());
        message.setSubject(notification.getSubject());

        LOGGER.debug("message from address::::"+message.getFromEmail());
        LOGGER.debug("message from name:::"+message.getFromName());
        LOGGER.debug("message to address:::"+message.getTo());
        try {
            MandrillMessageStatus[] messageStatusReports = mandrillApi
                    .messages().send(message, false);
            LOGGER.info(messageStatusReports.toString());
        } catch (MandrillApiError mandrillApiError) {
            mandrillApiError.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    @Transactional
    public Notification saveNotification(Notification notification) {
        LOGGER.info("********Saving the notification object*******");
        Notification n=notificationRepository.save(notification);
        LOGGER.info("notification id is:{}",n.getId());
        return n;
    }
    
}



