package com.piqube.pianalytics.service;

import com.piqube.pianalytics.model.AccessKeys;
import com.piqube.pianalytics.model.Users;

public interface AccessKeyService {
	 AccessKeys getUserByUserId (Users user);
	 AccessKeys updateAccessKey(AccessKeys accessKeys);
}
