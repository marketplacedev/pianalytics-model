package com.piqube.pianalytics.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.piqube.pianalytics.model.Authorities;
import com.piqube.pianalytics.repository.AuthoritiesRepository;
import com.piqube.pianalytics.repository.UserRepository;

/**
 * @author jenefar
 */
@Service
@Transactional
public class AuthoritiesServiceImpl implements AuthoritiesService{
    @Autowired
    AuthoritiesRepository authoritiesRepository;
    
    @Autowired
    UserRepository userRepository;

    @Override
    public void saveAuthority(Authorities authorities) {
        authoritiesRepository.save(authorities);
    }

    @Override
    public Set<Authorities> findByUsername(String username) { 
    	System.out.println(username+"::username");
        Set<Authorities> authorities=userRepository.findOneByUsername(username).getAuthorities();
        System.out.println(authorities.size()+":::auth");
        return authorities;
    }

   
}
