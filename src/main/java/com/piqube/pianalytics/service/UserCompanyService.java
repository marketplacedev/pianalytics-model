package com.piqube.pianalytics.service;



import java.util.List;

import com.piqube.pianalytics.model.UserCompany;

/**
 * @author jenefar
 */
public interface UserCompanyService {

	 UserCompany findByCompanyName(String companyName);
	 UserCompany createCompany(UserCompany company);

}
