package com.piqube.pianalytics.service;

import com.piqube.pianalytics.model.Notification;

public interface NotificationService {
	 String sendNotification(Notification notification);
	 Notification saveNotification(Notification notification);
}
