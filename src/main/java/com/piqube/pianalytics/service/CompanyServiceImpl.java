package com.piqube.pianalytics.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.piqube.pianalytics.model.Company;
import com.piqube.pianalytics.repository.CompanyRepository;



/**
 * @author jenefar
 */
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService{

    @Autowired
    CompanyRepository companyRepository;

    @Override
    public Company findByCompanyName(String name) {
        return companyRepository.findOneByName(name);
    }
}
