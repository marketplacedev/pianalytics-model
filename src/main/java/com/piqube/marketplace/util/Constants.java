package com.piqube.marketplace.util;

public class Constants {

    private Constants() {
    }

    public static final String EMAIL_EXISTS_MSG = "User with this email address already exists!";
    public static final String RECORD_NOT_FOUND_MSG="No user with this id exists!";
    public static final String FORGOT_PASSWORD_ERROR_MSG="No user with this email address exists. Please enter the email address with which you have signed up";
    public static final String AUTHORITY_NOT_FOUND="Please enter the correct authority name";
    public static final String NO_NOTIFICATION="No notification to view";

    public static final String WINDOWS_NOTIFICATION_JSON="C:/temp/notification.json";
    public static final String LINUX_NOTIFICATION_JSON="/tmp/notification.json";

    public static final String WINDOWS_AWS_PROPERTIES="C:/temp/aws.properties";
    public static final String LINUX_AWS_PROPERTIES="/tmp/aws.properties";
    public static final String COMPANY_NAME = "Please enter the company name";
    public static final String EMAIL = "Please enter the email address";
    public static final String COMPANY_REGISTER = "This company is not registered with us";
    public static final String ANONYMOUS_USER = "Anonymous User";
    public static final String EMAIL_NOTVERIFIED = "Email not verified";
    public static final String EMAIL_VERIFIED = "Email verified but not completed the onboard";    


}
