package com.piqube.marketplace.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.google.gson.Gson;
import com.piqube.pianalytics.model.Notification;
import com.piqube.pianalytics.vo.NotificationMessageVO;
import com.piqube.pianalytics.vo.NotificationVO;

import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.language.DefaultTemplateLexer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Helper {
    private static final Logger LOGGER = LoggerFactory.getLogger(Helper.class);
    static final HttpParams httpParams = new BasicHttpParams();


    public static Map<String, Object> convertJsonStringTOMap(String json){
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<>();
        // convert JSON string to Map
        try {
            map = mapper.readValue( json, new TypeReference<Map<String, Object>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(map);
        return map;
    }

    public static Object convertJsonToJava(String json, Class className) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        Object obj = null;
        try {
            obj = className.newInstance();
            obj = mapper.readValue(json, className);

        } catch (IOException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return obj;

    }


    public static String getMessageText(Notification notification, NotificationVO notificationProperties){
        LOGGER.info("In app Message Text"+notificationProperties.getInApp().getMessage());
        StringTemplate message = new StringTemplate(notificationProperties.getInApp().getMessage(), DefaultTemplateLexer.class);

        Map<String, Object> properties = Helper.convertJsonStringTOMap(notification.getModel());
        Iterator it = properties.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(pair.getValue()!=null)
                message.setAttribute(pair.getKey().toString(), pair.getValue().toString());
        }

        return message.toString();
    }

    public static String getSubjectText(Notification notification, NotificationVO notificationProperties){
        LOGGER.info("In app Subject Text"+notificationProperties.getInApp().getSubject());
        StringTemplate message = new StringTemplate(notificationProperties.getInApp().getSubject(), DefaultTemplateLexer.class);
        System.out.println(message.toString());


        Map<String, Object> properties = Helper.convertJsonStringTOMap(notification.getModel());
        Iterator it = properties.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if(pair.getValue()!=null)
                message.setAttribute(pair.getKey().toString(), pair.getValue().toString());
        }

        return message.toString();
    }
    public String readNotificationProperites(String notificationJsonProperties) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        /*File f;
        if(SystemUtils.IS_OS_WINDOWS)
            f = new File(Constants.WINDOWS_NOTIFICATION_JSON);
        else
            f = new File(Constants.LINUX_NOTIFICATION_JSON);

        InputStream inputStream = null;
        if (f.exists() && !f.isDirectory()) {
            try {
                inputStream = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            inputStream = classloader.getResourceAsStream("notification.json");
        }*/
        InputStream inputStream = classloader.getResourceAsStream(notificationJsonProperties);

        String notificationJson = null;
        try {
            notificationJson = IOUtils.toString(inputStream, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*// InputStream is = classloader.getResourceAsStream("test.csv");
        String notificationJson = null;
        try {
            notificationJson = FileUtils.readFileToString(new File(classloader.getResource("notification.json").getFile()));
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        return notificationJson;
    }

    public static String readFAQJson() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = null;
        inputStream = classloader.getResourceAsStream("faq.json");

        String faqJson = null;
        try {
            faqJson = IOUtils.toString(inputStream, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return faqJson;
    }
    public static String convertObjectToJson(Object obj){
        ObjectMapper mapper = new ObjectMapper();

        //Convert object to JSON string
        String model = null;
        try {
            model = mapper.writeValueAsString(obj);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(model);
        return model;
    }

    public static void postNotification(NotificationMessageVO messageVO) throws Exception {

        LOGGER.debug("Notification POST is called::::");
        System.out.println("Notification POST is called::::");
        HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
        //client = new DefaultHttpClient(httpParams);
        HttpClient client = new DefaultHttpClient(httpParams);

        String url=messageVO.getNotificationUrl();
        Gson gson= new Gson();
        HttpPost post = new HttpPost(url);
        StringEntity postingString =new StringEntity(gson.toJson(messageVO));
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        HttpResponse response = client.execute(post);
        LOGGER.debug("response is:::::"+response);
        System.out.println("response is:::::"+response);

    }
}
